FROM node:10.13.0 as builder

RUN mkdir -p /opt/app
WORKDIR /opt/app

COPY package.json package-lock.json ./
RUN npm install

COPY ./ ./

EXPOSE 3030

CMD ["npm", "start"]

